#!/bin/bash

for i in {0..31}; do
    sed -i "s/ *CAL_EDGE_FINE_DELAY *= *\".*\"/              CAL_EDGE_FINE_DELAY    =     \"$i\"/" CMSIT_RD53B.xml
    CMSITminiDAQ -f CMSIT_RD53B.xml -c pixelalive
    # BELOW WORKS ONLY IF STARTED WITH EMPTY RESULTS REPOSITORY AND RunNumber.txt HAS A ZERO
    padded_i=$(printf "%06d" "$i")
    CMSITminiDAQ -f CMSIT_RD53B.xml -b Results/Run${padded_i}_Physics_Board000.raw
done
