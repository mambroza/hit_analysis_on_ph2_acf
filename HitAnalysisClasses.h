#include "TString.h"
#include "TChain.h"
#include "TH2D.h"
#include "TFile.h"
#include "TCollection.h"
#include "TKey.h"
#include "TCanvas.h"
#include "TObjString.h"

// Copied from https://gitlab.cern.ch/cms_tk_ph2/eudaq/-/blob/cms_tk_master/user/CMSIT/module/src/CMSITConverterPlugin.cc
TH2D* FindHistogram(const std::string& nameInHisto, uint16_t hybridId, uint16_t chipId)
{
    TDirectory* dir = gDirectory;
    TKey*       key = nullptr;

    // ########################
    // # Find sub-directories #
    // ########################
    while(true)
    {
        TIter keyList(dir->GetListOfKeys());
        if(((key = (TKey*)keyList.Next()) != nullptr) && (key->IsFolder() == true) &&
           (std::string(key->GetName()).find(std::string("Hybrid")) == std::string::npos))
        {
            dir->cd(key->GetName());
            dir = gDirectory;
        }
        else
            break;
    }

    // #####################
    // # Search for Hybrid #
    // #####################
    TIter keyListHybrid(dir->GetListOfKeys());
    while((key != nullptr) && (key->IsFolder() == true) &&
          (std::string(key->GetName()).find(std::string("Hybrid_") + std::to_string(hybridId)) == std::string::npos))
    {
        key = (TKey*)keyListHybrid.Next();
    }

    // ###################
    // # Search for Chip #
    // ###################
    if(key != nullptr)
    {
        dir->cd(key->GetName());
        dir = gDirectory;
    }
    TIter keyListChip(dir->GetListOfKeys());
    while((key != nullptr) && (key->IsFolder() == true) &&
          (std::string(key->GetName()).find(std::string("Chip_") + std::to_string(chipId)) == std::string::npos))
    {
        key = (TKey*)keyListChip.Next();
    }

    // ###########################
    // # Enter in Chip directory #
    // ###########################
    if(key != nullptr) dir->cd(key->GetName());
    dir = gDirectory;
    TIter keyListHisto(dir->GetListOfKeys());

    // ######################
    // # Find the histogram #
    // ######################
    while(((key = (TKey*)keyListHisto.Next()) != nullptr) &&
          (std::string(key->GetName()).find(nameInHisto) == std::string::npos))
    {};
    if((dir != nullptr) && (dir->Get(key->GetName()) != nullptr))
    {
        return (TH2D*)((TCanvas*)dir->Get(key->GetName()))->GetPrimitive(key->GetName());
    }

    return nullptr;
}// FindHistogram

std::vector<TString> SplitString(TString string, TString delim)
{
    TObjArray *tx = string.Tokenize(delim);
    std::vector<TString> strings;
    for (Int_t i=0; i<tx->GetEntries(); i++)
    {
        strings.push_back(((TObjString *)(tx->At(i)))->String());
    }
    return strings;
}// SplitString


class Gain
{
private:
    TH2D *slope;
    TH2D *offset;
    bool setUp = false;

public:
    Gain(TString filename)
    {
        if (filename == "") return;   
        TFile *f = TFile::Open(filename);
        f->cd();
        if (f)
        {
            slope = FindHistogram("SlopeLowQ2D", 0, 15);
            f->cd();
            offset = FindHistogram("InterceptLowQ2D", 0, 15);
	    setUp = true;
        }
        else throw std::runtime_error(("File "+filename+" not found!").Data());
    }

    bool IsSetUp() const { return setUp; }

    double GetSlope(const int row, const int col) const
    {
        if (!setUp) return 0.0;
        return slope->GetBinContent(col+1, row+1);
    }

    double GetOffset(const int row, const int col) const
    {
        if (!setUp) return 0.0;
        return offset->GetBinContent(col+1, row+1);
    }

    double GetCharge(const int row, const int col, const int tot) const
    {
        if (!setUp) return 0.0;
            
        double slo = GetSlope(row, col);
        double off = GetOffset(row, col);
        if (slo <= 0) return 0;
        // if (off >= 0) return 0;
        return (tot - off) / slo;
    }

};// Class Gain


class Hit
{
private:
    uint16_t row;
    uint16_t col;
    uint8_t tot;

public:
    Hit(uint16_t _row, uint16_t _col, uint8_t _tot) :
        row(_row), col(_col), tot(_tot)
    {}

    int GetRow() const {return row;}
    int GetCol() const {return col;}
    int GetToT() const {return tot;}

    double GetCharge(const Gain gain) const
    {
        return gain.GetCharge(row, col, tot) * 5;
    }

    double GetChargeVCal(const Gain gain) const
    {
        return gain.GetCharge(row, col, tot);
    }

    bool IsAdjacent(const Hit other) const
    {
        return (std::abs(row-other.row)+std::abs(col-other.col)) == 1;
    }

    double Distance(const Hit other) const
    {
        double dCol = col - other.GetCol();
        double dRow = row - other.GetRow();
        return std::sqrt(dCol*dCol + dRow*dRow);
    }

    bool operator == (Hit const &other) const
    {
        return (row == other.GetRow() && col == other.GetCol() && tot == other.GetToT());
    }

    bool operator != (Hit const &other) const
    {
        return (row != other.GetRow() || col != other.GetCol() || tot != other.GetToT());
    }
};// Class Hit


class Cluster
{
private:
    uint32_t nHits = 0;
    std::vector<Hit> hits = {};

public:
    Cluster(){}

    Cluster(std::vector<Hit> _hits) : hits(_hits)
    {
        nHits = hits.size();
    }

    uint32_t GetNhits() const {return nHits;}
    uint32_t GetSize() const {return nHits;}

    std::vector<Hit> GetHits() const {return hits;}

    double GetRow(const Gain gain) const
    {
        double weightedRow = 0;
        for (auto& hit : hits)
        {
            weightedRow += hit.GetRow() * hit.GetCharge(gain);
        }
        return weightedRow/GetCharge(gain);
    }

    double GetCol(const Gain gain) const
    {
        double weightedCol = 0;
        for (auto& hit : hits)
        {
            weightedCol += hit.GetCol() * hit.GetCharge(gain);
        }
        return weightedCol/GetCharge(gain);
    }

    double GetCharge(const Gain gain) const
    {
        double charge = 0;
        for (auto& hit : hits)
        {
            charge += hit.GetCharge(gain);
        }
        return charge;
    }

    double GetChargeVCal(const Gain gain) const
    {
        double charge = 0;
	for (auto& hit : hits)
	{
            charge += hit.GetChargeVCal(gain);
	}
	return charge;
    } 

    bool IsAdjacent(const Hit other) const
    {
        for (auto& hit : hits)
        {
            if (hit != other && hit.IsAdjacent(other))
            {
                return true;
            }
        }
        return false;
    }
    
    bool IsAdjacent(const Cluster other) const
    {
        bool isAdjacent = false;
        for (auto& hit : hits)
        {
            for (auto& otherHit : other.GetHits())
            {
                if (hit == otherHit)
                {
                    return false;
                }
                else if (hit.IsAdjacent(otherHit))
                {
                    isAdjacent = true;
                }
            }
        }
        return isAdjacent;
    }

    bool Belongs(const Hit other) const
    {
        for (auto& hit : hits)
        {
            if (hit == other)
            {
                return true;
            }
        }
        return false;
    }

    int BelongsOrIsAdjacent(const Hit other) const // 1=Belongs, 2=IsAdjacent
    {
        int output = 0;
        for (auto& hit : hits)
        {
            if (hit == other)
            {
                return 1;
            }
            else if (hit.IsAdjacent(other))
            {
                output = 2;
            }
        }
        return output;
    }

    bool IsOverlapping(const Cluster other) const
    {
        for (auto& hit : hits)
        {
            for (auto& otherHit : other.GetHits())
            {
                if (hit == otherHit)
                {
                    return true;
                }
            }
        }
        return false;
    }

    bool IsOverlappingOrAdjacent(const Cluster other) const
    {
        for (auto& hit : hits)
        {
            for (auto& otherHit : other.GetHits())
            {
                if (hit == otherHit || hit.IsAdjacent(otherHit))
                {
                    return true;
                }
            }
        }
        return false;
    }

    void AddHit(Hit hit)
    {
        nHits++;
        hits.push_back(hit);
    }

    void Merge(Cluster other)
    {
        for (auto& hit: other.GetHits())
        {
            if (!Belongs(hit))
            {
                nHits++;
                hits.push_back(hit);
            }
        }
    }

    double ApproxDistance(const Cluster other) const
    {
        if (!nHits || !other.GetNhits()) return -1;
        double dCol = hits[0].GetCol() - other.GetHits()[0].GetCol();
        double dRow = hits[0].GetRow() - other.GetHits()[0].GetRow();
        return std::sqrt(dCol*dCol + dRow*dRow);
    }

    double ApproxDistance(const Hit other) const
    {
        if (!nHits) return -1;
        double dCol = hits[0].GetCol() - other.GetCol();
        double dRow = hits[0].GetRow() - other.GetRow();
        return std::sqrt(dCol*dCol + dRow*dRow);
    }

};// Class Cluster


class FrameEvent
{
private:
    uint16_t error_code;
    uint16_t hybrid_id;
    uint16_t chip_lane;
    uint16_t l1a_data_size;
    uint16_t chip_type;
    uint16_t frame_delay;
    uint16_t chip_id_mod4;
    uint16_t trigger_id;
    uint16_t trigger_tag;
    uint16_t bc_id;
    uint32_t status;
    uint32_t nhits;

    std::vector<Hit> hits = {};
    std::vector<Cluster> clusters = {};

    void BuildCluster(Cluster &clust, uint32_t i_hit, std::vector<bool> &used)
    {
        // std::cout << "  BuildCluster" << std::endl;
        clust.AddHit(hits[i_hit]);
        used[i_hit] = true;
        for (uint32_t i=0; i<nhits; ++i)
        {
            if (!used[i])
            {
                if (hits[i_hit].IsAdjacent(hits[i]))
                {                    
                    BuildCluster(clust, i, used);
                }
            }
        }
    }

    void BuildClusters()
    {
        // std::cout << "BuildClusters" << std::endl;
        std::vector<bool> used(nhits, false);
        for (uint32_t i=0; i<nhits; ++i)
        {
            if (!used[i])
            {
                Cluster cluster;
                BuildCluster(cluster, i, used);
                clusters.push_back(cluster);
            }
        }
        return;
    }

public:
    FrameEvent(uint16_t FW_frame_event_error_code,
               uint16_t FW_frame_event_hybrid_id,
               uint16_t FW_frame_event_chip_lane,
               uint16_t FW_frame_event_l1a_data_size,
               uint16_t FW_frame_event_chip_type,
               uint16_t FW_frame_event_frame_delay,
               uint16_t RD53_frame_event_chip_id_mod4,
               uint16_t RD53_frame_event_trigger_id,
               uint16_t RD53_frame_event_trigger_tag,
               uint16_t RD53_frame_event_bc_id,
               uint32_t RD53_frame_event_status,
               uint32_t RD53_frame_event_nhits,
               std::vector<uint16_t>  event_hit_row,
               std::vector<uint16_t>  event_hit_col,
               std::vector<uint8_t>   event_hit_tot
    ) : error_code(FW_frame_event_error_code),
        hybrid_id(FW_frame_event_hybrid_id),
        chip_lane(FW_frame_event_chip_lane),
        l1a_data_size(FW_frame_event_l1a_data_size),
        chip_type(FW_frame_event_chip_type),
        frame_delay(FW_frame_event_frame_delay),
        chip_id_mod4(RD53_frame_event_chip_id_mod4),
        trigger_id(RD53_frame_event_trigger_id),
        trigger_tag(RD53_frame_event_trigger_tag),
        bc_id(RD53_frame_event_bc_id),
        status(RD53_frame_event_status),
        nhits(RD53_frame_event_nhits)
    {
        for (uint32_t i=0; i<nhits; ++i)
        {
            Hit hit(event_hit_row[i], event_hit_col[i], event_hit_tot[i]);
            hits.push_back(hit);
        }
        BuildClusters();
    }// Constructor

    uint16_t GetErrorCode() {return error_code;}
    uint16_t GetHybridId() {return hybrid_id;}
    uint16_t GetChipLane() {return chip_lane;}
    uint16_t GetL1aDataSize() {return l1a_data_size;}
    uint16_t GetChipType() {return chip_type;}
    uint16_t GetFrameDelay() {return frame_delay;}
    uint16_t GetChipIdMod4() {return chip_id_mod4;}
    uint16_t GetTriggerId() {return trigger_id;}
    uint16_t GetTriggerTag() {return trigger_tag;}
    uint16_t GetBcId() {return bc_id;}
    uint32_t GetStatus() {return status;}
    uint32_t GetNhits() {return nhits;}
    uint32_t GetNclusters() {return clusters.size();}

    std::vector<Hit> GetHits() {return hits;}
    std::vector<Cluster> GetClusters() {return clusters;}
};// Class FrameEvent


class Event
{
private:
    uint32_t number;
    uint32_t block_size;
    uint32_t tlu_trigger_id;
    uint32_t data_format_ver;
    uint32_t tdc;
    uint32_t l1a_counter;
    uint32_t bx_counter;
    uint32_t event_status;
    uint32_t nframes;
    std::vector<FrameEvent> frameEvents;

public:
    Event(uint32_t event,
	  uint32_t FW_block_size,
	  uint32_t FW_tlu_trigger_id,
	  uint32_t FW_data_format_ver,
	  uint32_t FW_tdc,
	  uint32_t FW_l1a_counter,
	  uint32_t FW_bx_counter,
	  uint32_t FW_event_status,
	  uint32_t FW_nframes,
	  std::vector<uint16_t> FW_frame_event_error_code,
          std::vector<uint16_t> FW_frame_event_hybrid_id,
          std::vector<uint16_t> FW_frame_event_chip_lane,
          std::vector<uint16_t> FW_frame_event_l1a_data_size,
          std::vector<uint16_t> FW_frame_event_chip_type,
          std::vector<uint16_t> FW_frame_event_frame_delay,
          std::vector<uint16_t> RD53_frame_event_chip_id_mod4,
          std::vector<uint16_t> RD53_frame_event_trigger_id,
          std::vector<uint16_t> RD53_frame_event_trigger_tag,
          std::vector<uint16_t> RD53_frame_event_bc_id,
          std::vector<uint32_t> RD53_frame_event_status,
          std::vector<uint32_t> RD53_frame_event_nhits,
          std::vector<uint16_t> RD53_hit_row,
          std::vector<uint16_t> RD53_hit_col,
          std::vector<uint8_t>  RD53_hit_tot
    )
    {
	number = event;
	block_size = FW_block_size;
	tlu_trigger_id = FW_tlu_trigger_id;
	data_format_ver = FW_data_format_ver;
	tdc = FW_tdc;
	l1a_counter = FW_l1a_counter;
	bx_counter = FW_bx_counter;
	event_status = FW_event_status;
	nframes = FW_nframes;
        unsigned int i_hit = 0;
        for (unsigned int i=0; i<FW_frame_event_error_code.size(); ++i)
        {
            std::vector<uint16_t> fe_hit_row(&RD53_hit_row[i_hit], &RD53_hit_row[i_hit+RD53_frame_event_nhits[i]]);
            std::vector<uint16_t> fe_hit_col(&RD53_hit_col[i_hit], &RD53_hit_col[i_hit+RD53_frame_event_nhits[i]]);
            std::vector<uint8_t>  fe_hit_tot(&RD53_hit_tot[i_hit], &RD53_hit_tot[i_hit+RD53_frame_event_nhits[i]]);
            i_hit += RD53_frame_event_nhits[i];

            FrameEvent fe(FW_frame_event_error_code[i],
                          FW_frame_event_hybrid_id[i],
                          FW_frame_event_chip_lane[i],
                          FW_frame_event_l1a_data_size[i],
                          FW_frame_event_chip_type[i],
                          FW_frame_event_frame_delay[i],
                          RD53_frame_event_chip_id_mod4[i],
                          RD53_frame_event_trigger_id[i],
                          RD53_frame_event_trigger_tag[i],
                          RD53_frame_event_bc_id[i],
                          RD53_frame_event_status[i],
                          RD53_frame_event_nhits[i],
                          fe_hit_row,
                          fe_hit_col,
                          fe_hit_tot
            );

            frameEvents.push_back(fe);
        }
    } // Constructor

    unsigned int GetNo() {return number;}

    unsigned int GetTDC() {return tdc;}

    unsigned int GetTluTriggerId() {return tlu_trigger_id;}

    unsigned int GetL1aCounter() {return l1a_counter;}

    unsigned int GetBxCounter() {return bx_counter;}

    unsigned int GetNframeEvents() {return frameEvents.size();}

    std::vector<FrameEvent> GetFrameEvents()
    {
        return frameEvents;
    }

    FrameEvent GetFrameEvent(unsigned int i)
    {
        return frameEvents[i];
    }
};// Class Event


class TreeReader
{
private:
    TChain *chain;
    
    uint32_t event;
    uint32_t FW_block_size;
    uint32_t FW_tlu_trigger_id;
    uint32_t FW_data_format_ver;
    uint32_t FW_tdc;
    uint32_t FW_l1a_counter;
    uint32_t FW_bx_counter;
    uint32_t FW_event_status;
    uint32_t FW_nframes;
    
    std::vector<uint16_t> *FW_frame_event_error_code;
    std::vector<uint16_t> *FW_frame_event_hybrid_id;
    std::vector<uint16_t> *FW_frame_event_chip_lane;
    std::vector<uint16_t> *FW_frame_event_l1a_data_size;
    std::vector<uint16_t> *FW_frame_event_chip_type;
    std::vector<uint16_t> *FW_frame_event_frame_delay;

    std::vector<uint16_t> *RD53_frame_event_chip_id_mod4;
    std::vector<uint16_t> *RD53_frame_event_trigger_id;
    std::vector<uint16_t> *RD53_frame_event_trigger_tag;
    std::vector<uint16_t> *RD53_frame_event_bc_id;
    std::vector<uint32_t> *RD53_frame_event_status;
    std::vector<uint32_t> *RD53_frame_event_nhits;

    std::vector<uint16_t> *RD53_hit_row;
    std::vector<uint16_t> *RD53_hit_col;
    std::vector<uint8_t>  *RD53_hit_tot;

    void SetupBranches()
    {
	chain->SetBranchAddress("event", &event);
	chain->SetBranchAddress("FW_block_size", &FW_block_size);
	chain->SetBranchAddress("FW_tlu_trigger_id", &FW_tlu_trigger_id);
	chain->SetBranchAddress("FW_data_format_ver", &FW_data_format_ver);
	chain->SetBranchAddress("FW_tdc", &FW_tdc);
	chain->SetBranchAddress("FW_l1a_counter", &FW_l1a_counter);
	chain->SetBranchAddress("FW_bx_counter", &FW_bx_counter);
	chain->SetBranchAddress("FW_event_status", &FW_event_status);
	chain->SetBranchAddress("FW_nframes", &FW_nframes);
        chain->SetBranchAddress("FW_frame_event_error_code",     &FW_frame_event_error_code);
        chain->SetBranchAddress("FW_frame_event_hybrid_id",      &FW_frame_event_hybrid_id);
        chain->SetBranchAddress("FW_frame_event_chip_lane",      &FW_frame_event_chip_lane);
        chain->SetBranchAddress("FW_frame_event_l1a_data_size",  &FW_frame_event_l1a_data_size);
        chain->SetBranchAddress("FW_frame_event_chip_type",      &FW_frame_event_chip_type);
        chain->SetBranchAddress("FW_frame_event_frame_delay",    &FW_frame_event_frame_delay);

        chain->SetBranchAddress("RD53_frame_event_chip_id_mod4", &RD53_frame_event_chip_id_mod4);
        chain->SetBranchAddress("RD53_frame_event_trigger_id",   &RD53_frame_event_trigger_id);
        chain->SetBranchAddress("RD53_frame_event_trigger_tag",  &RD53_frame_event_trigger_tag);
        chain->SetBranchAddress("RD53_frame_event_bc_id",        &RD53_frame_event_bc_id);
        chain->SetBranchAddress("RD53_frame_event_status",       &RD53_frame_event_status);
        chain->SetBranchAddress("RD53_frame_event_nhits",        &RD53_frame_event_nhits);

        chain->SetBranchAddress("RD53_hit_row",                  &RD53_hit_row);
        chain->SetBranchAddress("RD53_hit_col",                  &RD53_hit_col);
        chain->SetBranchAddress("RD53_hit_tot",                  &RD53_hit_tot);

	chain->SetBranchStatus("event", 1);
	chain->SetBranchStatus("FW_block_size", 1);
	chain->SetBranchStatus("FW_tlu_trigger_id", 1);
	chain->SetBranchStatus("FW_data_format_ver", 1);
	chain->SetBranchStatus("FW_tdc", 1);
	chain->SetBranchStatus("FW_l1a_counter", 1);
	chain->SetBranchStatus("FW_bx_counter", 1);
	chain->SetBranchStatus("FW_event_status", 1);
	chain->SetBranchStatus("FW_nframes", 1);
        chain->SetBranchStatus("FW_frame_event_error_code",     1);
        chain->SetBranchStatus("FW_frame_event_hybrid_id",      1);
        chain->SetBranchStatus("FW_frame_event_chip_lane",      1);
        chain->SetBranchStatus("FW_frame_event_l1a_data_size",  1);
        chain->SetBranchStatus("FW_frame_event_chip_type",      1);
        chain->SetBranchStatus("FW_frame_event_frame_delay",    1);

        chain->SetBranchStatus("RD53_frame_event_chip_id_mod4", 1);
        chain->SetBranchStatus("RD53_frame_event_trigger_id",   1);
        chain->SetBranchStatus("RD53_frame_event_trigger_tag",  1);
        chain->SetBranchStatus("RD53_frame_event_bc_id",        1);
        chain->SetBranchStatus("RD53_frame_event_status",       1);
        chain->SetBranchStatus("RD53_frame_event_nhits",        1);

        chain->SetBranchStatus("RD53_hit_row",                  1);
        chain->SetBranchStatus("RD53_hit_col",                  1);
        chain->SetBranchStatus("RD53_hit_tot",                  1);
    } // SetupBranches

public:
    TreeReader(TString treename, TString fileNames)
    {        
        fileNames.ReplaceAll(", ", " ");
        fileNames.ReplaceAll("; ", " ");
        fileNames.ReplaceAll("\n", " ");
        fileNames.ReplaceAll("\t", " ");
        fileNames.ReplaceAll(",", " ");
        fileNames.ReplaceAll(";", " ");
        std::vector<TString> filenames = SplitString(fileNames, " ");

        chain = new TChain(treename);
        for (auto& filename : filenames)
        {
            chain->Add(filename);
        }
        FW_frame_event_error_code = new std::vector<uint16_t>;
        FW_frame_event_hybrid_id = new std::vector<uint16_t>;
        FW_frame_event_chip_lane = new std::vector<uint16_t>;
        FW_frame_event_l1a_data_size = new std::vector<uint16_t>;
        FW_frame_event_chip_type = new std::vector<uint16_t>;
        FW_frame_event_frame_delay = new std::vector<uint16_t>;

        RD53_frame_event_chip_id_mod4 = new std::vector<uint16_t>;
        RD53_frame_event_trigger_id = new std::vector<uint16_t>;
        RD53_frame_event_trigger_tag = new std::vector<uint16_t>;
        RD53_frame_event_bc_id = new std::vector<uint16_t>;
        RD53_frame_event_status = new std::vector<uint32_t>;
        RD53_frame_event_nhits = new std::vector<uint32_t>;

        RD53_hit_row = new std::vector<uint16_t>;
        RD53_hit_col = new std::vector<uint16_t>;
        RD53_hit_tot = new std::vector<uint8_t>;

        SetupBranches();
    }

    int GetNevents() {return chain->GetEntries();}

    Event GetEvent(int iEntry)
    {
        chain->GetEntry(iEntry);
        Event evt(event,
		  FW_block_size,
		  FW_tlu_trigger_id,
		  FW_data_format_ver,
		  FW_tdc,
		  FW_l1a_counter,
		  FW_bx_counter,
		  FW_event_status,
		  FW_nframes,
		  *FW_frame_event_error_code,
                  *FW_frame_event_hybrid_id,
                  *FW_frame_event_chip_lane,
                  *FW_frame_event_l1a_data_size,
                  *FW_frame_event_chip_type,
                  *FW_frame_event_frame_delay,
                  *RD53_frame_event_chip_id_mod4,
                  *RD53_frame_event_trigger_id,
                  *RD53_frame_event_trigger_tag,
                  *RD53_frame_event_bc_id,
                  *RD53_frame_event_status,
                  *RD53_frame_event_nhits,
                  *RD53_hit_row,
                  *RD53_hit_col,
                  *RD53_hit_tot
        );
        return evt;
    } // GetEvent

};// Class TreeReader


class ProgressBar
{
private:
    Double_t nume, deno, progress;

public:
    ProgressBar(UInt_t iMax=0) :
        nume(0), deno(0), progress(0)
    {
        if (!iMax) return;
        deno = iMax;
    }

    void Draw(UInt_t iEntry=0)
    {
        if (!iEntry && !deno) {cout << "0 events selected.\n"; return;}
        nume = iEntry;
        if (deno<nume || nume<0) {cout << "Wrong iEntry.\n"; return;}
        else {
            progress = 51*nume/deno;
            // Check progress so it doesnt redraw the same picture over and over
            if(progress-51/deno<round(progress) && progress>=round(progress)) {
                std::cout << "\r|";
                for (int n=1; n<=50; n++){
                    if (n<=progress) { std::cout << "+";}
                    else std::cout << "-";
                }
                std::cout << "|  " << round(progress*1.97) << "% ";
                std::cout.flush();
            }
            if (nume==deno-1) std::cout << "\r        Finished.                                                         \n";
        }
    }// Draw()

};// Class ProgressBar
