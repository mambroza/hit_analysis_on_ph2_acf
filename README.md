# hit_analysis_on_ph2_acf
This repository contains the scripts that analyse the ROOT files with chip hit data created by Ph2_ACF.
It can build clusters, plot hit maps, cluster maps, charge distributions, and do all that separately for different trigger IDs, if one is using `nTRIGxEvent`>1.

## Getting started
You can clone this repository straight into your copy of [Ph2_ACF](https://gitlab.cern.ch/cmsinnertracker/Ph2_ACF/-/tree/master?ref_type=heads) or anywhere you want.
```
git clone https://gitlab.cern.ch/mambroza/hit_analysis_on_ph2_acf.git
```

### Simple analysis
1. Do the procedure you want in Ph2_ACF and make sure you are saving the hits into a binary file by setting the following in the xml file:
```
<Setting name="SaveBinaryData">        1 </Setting>
```
An example xml file is given in this repository.

2. Convert the produced binary file into a ROOT file:
```
CMSITminiDAQ -f CMSIT_RD53B.xml -b Results/Run000000_physics_Board000.raw
```

3. Run the main analysis script like this:
```
./HitAnalysis -f Results/Run000000_physics_Board000.raw
```

4. There are a lot more things you can set for the `HitAnalysis` script. You can check them using the `-h` flag:
```
./HitAnalysis -h
```
One important flag to set might be `-n NUMBER`, where `NUMBER` must match the `nTRIGxEvent` parameter used when producing the data.

### Fine delay scan using pixelalive and HitOR trigger
1. Properly set the xml file to use the triggering scheme of your choice (Test FSM, external HitOR, HitOR self-trigger) and make sure that `nTRIGxEvent` is set to 10 (also make sure that the latency is approximately correct for your triggering scheme).
2. Make sure that the name of the xml file you intend to use is exactly `CMSIT_RD53B.xml`.
3. Make sure that the `Results` directory inside your working directory is empty and that the file `RunNumber.txt` either contains a zero or does not exist at all:
```
mv Results Results_backup
rm RunNumber.txt
```
4. Run the `fineDelayScanScript.sh` (it just changes the `CAL_EDGE_FINE_DELAY` parameter inside the xml file, runs pixelalive multiple times, and turns all the raw files to ROOT files):
```
./fineDelayScanScript.sh
```
5. Once all the runs finish, run the `fineDelayAnalysisScript.sh` (it just runs `HitAnalysis` multiple times on all the root files and saves the plots in different directories):
```
./fineDelayAnalysisScript.sh
```
**Note here that the script will run quite slowly because it is trying to build clusters even with injected hits**.
I would suggest setting `nEvents` to some lower number (e.g., 10) when producing the data, so the analysis doesn't take that long.
</br>I will add the possibility to skip the hit clustering step if it is not needed (which is the case for this test) when I have time.

6. If you want to use different `nTRIGxEvent` values, different file names, etc., feel free to edit the `fineDelay*Script.sh` scripts.
