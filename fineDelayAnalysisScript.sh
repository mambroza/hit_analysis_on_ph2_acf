#!/bin/bash

# WORKS ONLY IF STARTED WITH EMPTY RESULTS REPOSITORY AND RunNumber.txt HAS A ZERO
for i in {0..31}; do
	padded_i=$(printf "%06d" "$i")
	./HitAnalysis -f Results/Run${padded_i}_*_Board000.root -n 10
	mkdir Delay${i}
	mv *.png Delay${i};
done
